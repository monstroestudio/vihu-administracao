import { VihuPage } from './app.po';

describe('vihu App', () => {
  let page: VihuPage;

  beforeEach(() => {
    page = new VihuPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
