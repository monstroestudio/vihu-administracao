import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  // static DOMAIN = 'http://localhost:8000/api';
  // static DOMAIN = 'https://api.vihu.com.br/api';
  static DOMAIN = 'http://api.vihu.ml/api';

  static set user(value:any){
    localStorage.setItem('user', JSON.stringify(value));
  }
  static get user(){
    return JSON.parse(localStorage.getItem('user'));
  }

  static set token(value:any){
    localStorage.setItem('token', JSON.stringify(value));
  }
  static get token(){
    return JSON.parse(localStorage.getItem('token'));
  }
}
