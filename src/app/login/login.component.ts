import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';
import { LoginService } from './login.service';

declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  data:any = {
    email: '',
    password: ''
  }
  errorMessage:string = '';
  warningMessage:string = '';  

  constructor(private router:Router, private loginService:LoginService) {
    
  }

  ngOnInit() {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  }

  login() {
    this.errorMessage = '';

    if (this.data.email == ''){
      this.errorMessage = 'Complete all fields';
      return;
    }

    if (this.data.password == ''){
      this.errorMessage = 'Complete all fields';
      return;
    }

    this.loginService.login(this.data).subscribe(
      response => {
        AppComponent.user = response.user;
        AppComponent.token = response.token;
        // this.router.navigate(['/admin']);
        document.location.href = '/admin';
      },
      error => {
        console.log(error.status);
        if (error.status == 401) {
          this.errorMessage = 'Login ou senha inválido';  
        } else {
          let body = JSON.parse(error._body);
          this.errorMessage = body.error;
        }
      }
    )
  }
}
