import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { BlocosService } from './../blocos.service';
import { CondominiosService } from './../../condominios/condominios.service';

declare var $:any;

@Component({
  selector: 'app-bloco-form',
  templateUrl: './bloco-form.component.html',
  styleUrls: ['./bloco-form.component.scss'],
  providers: [BlocosService, CondominiosService]
})
export class BlocoFormComponent implements OnInit {

  data:any = {
    condominiums_id: '',
    name: '',
    floors: ''
  }
  consominios:any[] = [];

  constructor(private blocosService:BlocosService, private consominiosService:CondominiosService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.blocosService.show(params.id).subscribe(
            response => {
              this.data = response.block;
            },
            error => console.error(error)
          );
        }
      }
    );    
  }

  ngOnInit() {
    this.consominiosService.list().subscribe(
      response => {
        if (response.items > 0) this.consominios = response.data;
      },
      error => console.error(error)
    );    
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required, select.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    this.blocosService.save(this.data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/blocos']);
      },
      error => console.error(error)
    );
  }

  remove() {
    this.blocosService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/blocos']);
      },
      error => console.error(error)
    );    
  }
}
