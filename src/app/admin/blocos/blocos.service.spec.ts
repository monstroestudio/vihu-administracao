import { TestBed, inject } from '@angular/core/testing';

import { BlocosService } from './blocos.service';

describe('BlocosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlocosService]
    });
  });

  it('should be created', inject([BlocosService], (service: BlocosService) => {
    expect(service).toBeTruthy();
  }));
});
