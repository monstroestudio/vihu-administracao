import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BlocosService } from './blocos.service';

declare var $:any;

@Component({
  selector: 'app-blocos',
  templateUrl: './blocos.component.html',
  styleUrls: ['./blocos.component.scss'],
  providers: [BlocosService]
})
export class BlocosComponent implements OnInit {

  blocos:any[] = [];

  constructor(private router:Router, private blocosService:BlocosService) { }

  ngOnInit() {
    this.blocosService.list().subscribe(
        response => {
          this.blocos = response.blocks;
          setTimeout(()=>{ $("#blocos").DataTable(); }, 100);
        },
        error => console.error(error)
    );
  }

  open(item) {
    this.router.navigate(['/admin/blocos/'+item.id]);
  }  
}
