import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UsuariosService } from './../usuarios.service';
import { UnidadesService } from './../../unidades/unidades.service';
import { BlocosService } from './../../blocos/blocos.service';
import { CondominiosService } from './../../condominios/condominios.service';
import { RevendasService } from './../../revendas/revendas.service';

declare var $:any;

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.scss'],
  providers: [RevendasService, CondominiosService, BlocosService, UnidadesService, UsuariosService]
})
export class UsuarioFormComponent implements OnInit {

  data:any = {
    name: '',
    password: '',
    email: '',
    mobile: '',
    phone: '',
    is_admin: 'N',
    rfid_keys: [{code: ''}],
    communication_type: {
      sms: false,
      ura: false,
      intercom: false
    },
    units: [{
      block_id: '',
      condominiums_id: '',
      type: 'morador'
    }]
  }
  condominios:any[] = [];  
  blocos:any[] = [];
  unidades:any[] = [];
  saving:boolean;

  constructor(
    private revendasService:RevendasService, 
    private condominiosService:CondominiosService, 
    private blocosService:BlocosService, 
    private unidadesService:UnidadesService, 
    private usuariosService:UsuariosService,
    private router:Router,
    private route:ActivatedRoute
  ) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.usuariosService.show(params.id).subscribe(
            response => {
              this.data = response.user;
              if (!this.data.communication_type) this.data.communication_type = {sms: false, ura: false, intercom: false};
              if (!this.data.rfid_keys) this.data.rfid_keys = [{code: ''}];

              if (this.data.units.length == 0){
                this.addUnit();
              } else {
                this.data.units.forEach(unit => {
                  this.data.types.forEach(type => {
                    if (unit.condominiums_id == type.condominiums_id) {
                      unit.type = type.type;
                    }
                  });
                });
              }

              setTimeout(() => {
                $('#celular, #telefone').trigger('input');
                $('input[type="radio"], input[type="checkbox"]').iCheck('update');
              }, 100);
            },
            error => console.error(error)
          );
        }
      }
    );
  }

  ngOnInit() {
    this.condominiosService.list().subscribe(
      response => {
        this.condominios = response.data;
      },
      error => console.error(error)
    );

    this.blocosService.list().subscribe(
      response => {
        this.blocos = response.blocks;
      },
      error => console.error(error)
    );

    this.unidadesService.list().subscribe(
      response => {
        this.unidades = response.data;
      },
      error => console.error(error)
    );

    $('#celular').mask('(00) 00000-0000', {clearIfNotMatch: true});
    $('#telefone').mask('(00) 0000-0000', {clearIfNotMatch: true});

    $('input[type="checkbox"], input[type="radio"]').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue'
    });
    $('input[type="radio"]').on('ifClicked', (event)=>{
      this.data.is_admin = event.target.value;
    });
    setTimeout(() => {
      $('input[type="radio"]').iCheck('update');
    }, 100);    
  }

  filterBlocks(unit) {
    if (unit == '') return [];

    return this.blocos.filter((v) => {
      if (v.condominiums_id == unit.condominiums_id)
        return true;
      else
        return false;
    });
  }

  filterUnits(unit) {
    if (unit == '') return [];

    return this.unidades.filter((v) => {
      if (v.block_id == unit.block_id && v.condominiums_id == unit.condominiums_id) 
        return true;
      else
        return false;
    });
  }

  addUnit() {
    this.data.units.push({
      id: '',
      block_id: '',
      condominiums_id: '',
      type: 'morador'
    });    
  }

  removeUnit(index) {
    this.data.units.splice(index, 1);
  }

  addKey() {
    this.data.rfid_keys.push({code:''});
  }

  removeKey(index) {
    this.data.rfid_keys.splice(index, 1);
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required, select.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    let _data = JSON.parse(JSON.stringify(this.data));
    if (_data.mobile) _data.mobile = _data.mobile.replace(/\D/g,'');
    if (_data.phone) _data.phone = _data.phone.replace(/\D/g,'');

    this.saving = true;
    this.usuariosService.save(_data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.Message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        
        this.saving = false;
        this.router.navigate(['/admin/usuarios']);
      },
      error => {
        this.saving = false;
        
        let msg = JSON.parse(error._body).message;
        $.toast({
          heading: 'Erro',
          text: msg,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'error'
        });        
      }
    );
  }

  remove() {
    this.usuariosService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/usuarios']);
      },
      error => console.error(error)
    );    
  }
}
