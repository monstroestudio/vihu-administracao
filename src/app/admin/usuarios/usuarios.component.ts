import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UsuariosService } from './usuarios.service';

declare var $:any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  providers: [UsuariosService]
})
export class UsuariosComponent implements OnInit {

  usuarios:any[] = [];

  constructor(private router:Router, private usuariosService:UsuariosService) { }

  ngOnInit() {
    this.usuariosService.list().subscribe(
        response => {
          this.usuarios = response.users;
          setTimeout(()=>{ $("#usuarios").DataTable(); }, 100);
        },
        error => console.error(error)
    );
  }

  getType(item) {
    let result:string = '';

    if (item.types.length > 1) {
      item.types.forEach(type => {
        result += type.type + ' / '
      });
      
      result = result.substr(0, result.length - 3);
    } else if (item.types.length == 1) {
      result = item.types[0].type
    } else {
      result = "";
    }
    
    return result;
  }

  open(item) {
    this.router.navigate(['/admin/usuarios/'+item.id]);
  }
}
