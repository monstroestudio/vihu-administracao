import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppComponent } from '../../app.component';

@Injectable()
export class UsuariosService {

  private headers:Headers;

  constructor(private http:Http) {
     this.headers = new Headers();
     this.headers.append('Content-Type', 'application/json');
     this.headers.append('Authorization', 'Bearer ' + AppComponent.token);
  }

  list(){
		return this.http.get(AppComponent.DOMAIN + '/users', {
			headers: this.headers
		}).map( response => response.json() );
  }

  show(id) {
		return this.http.get(AppComponent.DOMAIN + '/users/'+id, {
			headers: this.headers
		}).map( response => response.json() );    
  }

  save(data) {
    if (data.id) {
      return this.http.put(AppComponent.DOMAIN + '/users/' + data.id, data, {
        headers: this.headers
      }).map( response => response.json() );          
    } else {    
      return this.http.post(AppComponent.DOMAIN + '/users', data, {
        headers: this.headers
      }).map( response => response.json() );
    }
  }

  remove(id) {
		return this.http.delete(AppComponent.DOMAIN + '/users/'+id, {
			headers: this.headers
		}).map( response => response.json() );    
  }
}