import { TestBed, inject } from '@angular/core/testing';

import { LogAcessoService } from './log-acesso.service';

describe('LogAcessoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogAcessoService]
    });
  });

  it('should be created', inject([LogAcessoService], (service: LogAcessoService) => {
    expect(service).toBeTruthy();
  }));
});
