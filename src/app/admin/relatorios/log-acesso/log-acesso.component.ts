import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LogAcessoService } from './log-acesso.service';

declare var $:any;

@Component({
  selector: 'app-log-acesso',
  templateUrl: './log-acesso.component.html',
  styleUrls: ['./log-acesso.component.scss'],
  providers: [LogAcessoService]
})
export class LogAcessoComponent implements OnInit {

  acessos:any[] = [];

  constructor(private router:Router, private logAcessoService:LogAcessoService) { }

  ngOnInit() {
    this.logAcessoService.list().subscribe(
      response => {
        this.acessos = response.data;
        setTimeout(()=>{ 
          $("#logAcessos").DataTable({
            "order": [[ 7, "desc" ]]
          });
        }, 100);
      },
      error => console.error(error)
    )    
  }

  getColor(status:string) {
    if (status == 'valid')
      return 'label-success';
    else if (status == 'invalid')
      return 'label-danger';
    else if (status == 'expired')
      return 'label-warning';
    else if (status == 'gate_repeated')
      return 'label-warning';    
    else if (status == 'ip_invalid')
      return 'label-danger';    
  }

  getStatus(status:string) {
    if (status == 'valid')
      return 'OK';
    else if (status == 'invalid')
      return 'Inválido';
    else if (status == 'expired')
      return 'Expirado';
    else if (status == 'gate_repeated')
      return 'Portão inválido';
    else if (status == 'ip_invalid')
      return 'IP Inválido';    
  }  
}
