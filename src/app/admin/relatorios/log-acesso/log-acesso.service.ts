import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppComponent } from '../../../app.component';

@Injectable()
export class LogAcessoService {

  private headers:Headers;

  constructor(private http:Http) {
     this.headers = new Headers();
     this.headers.append('Content-Type', 'application/json');
     this.headers.append('Authorization', 'Bearer ' + AppComponent.token);
  }

  list(){
		return this.http.get(AppComponent.DOMAIN + '/reports/accesses', {
			headers: this.headers
		}).map( response => response.json() );
  }
}
