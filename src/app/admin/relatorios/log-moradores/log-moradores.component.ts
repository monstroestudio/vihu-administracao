import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { LogMoradoresService } from './log-moradores.service';

declare var $:any;

@Component({
  selector: 'app-log-moradores',
  templateUrl: './log-moradores.component.html',
  styleUrls: ['./log-moradores.component.scss'],
  providers: [LogMoradoresService]
})
export class LogMoradoresComponent implements OnInit {

  acessos:any[] = [];

  constructor(private router:Router, private logMoradoresService:LogMoradoresService) { }

  ngOnInit() {
    this.logMoradoresService.list().subscribe(
      response => {
        this.acessos = response.data;
        setTimeout(()=>{ 
          $("#logMoradores").DataTable({
            "order": [[ 4, "desc" ]]
          });
        }, 100);
      },
      error => console.error(error)
    )    
  }

}
