import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogMoradoresComponent } from './log-moradores.component';

describe('LogMoradoresComponent', () => {
  let component: LogMoradoresComponent;
  let fixture: ComponentFixture<LogMoradoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogMoradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogMoradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
