import { TestBed, inject } from '@angular/core/testing';

import { LogMoradoresService } from './log-moradores.service';

describe('LogMoradoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogMoradoresService]
    });
  });

  it('should be created', inject([LogMoradoresService], (service: LogMoradoresService) => {
    expect(service).toBeTruthy();
  }));
});
