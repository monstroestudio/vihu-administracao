import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { CondominiosService } from './../condominios.service';
import { RevendasService } from './../../revendas/revendas.service';
import { AddressService } from './../../../shared/address/address.service';

declare var $:any;

@Component({
  selector: 'app-condominio-form',
  templateUrl: './condominio-form.component.html',
  styleUrls: ['./condominio-form.component.scss'],
  providers: [RevendasService, CondominiosService, AddressService]
})
export class CondominioFormComponent implements OnInit {

  data:any = {
    resale_id: '',
    name: '',
    address: '',
    number: '',
    complement: '',
    district: '',
    city: '',
    state: '',
    cep: '',
    status: 1,
    number_of_gates: '',
    dns: [{
      dns: '',
      ip: ''  
    }],
    communication_type:{
      sms: false,
      ura: false,
      intercom: false
    },
    application_features:{
      visitors: true,
      delivery: true,
      party: true,
      safeBox: true,
      reports: true
    },
    lat: '',
    lng: ''
  }
  resales:any[] = [];
  addressLoading:boolean;
  saving:boolean;

  constructor(private revendasService:RevendasService, private consominiosService:CondominiosService, private addressService:AddressService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.consominiosService.show(params.id).subscribe(
            response => {
              this.data = response.condominium;
              this.data.status = (this.data.status == 'ATIVO') ? 1 : 2;
              
              if (!this.data.communication_type) {
                this.data.communication_type = {
                  sms: false,
                  ura: false,
                  intercom: false
                };
              }

              if (!this.data.application_features) {
                this.data.application_features = {
                  visitors: true,
                  delivery: true,
                  party: true,
                  safeBox: true,
                  reports: true
                };
              }

              setTimeout(()=>{$('input[type="checkbox"]').iCheck('update')},100);
            },
            error => console.error(error)
          );
        } else {
          setTimeout(()=>{$('input[type="checkbox"]').iCheck('update')},100);          
        }
      }
    );
  }

  ngOnInit() {
    this.revendasService.list().subscribe(
      response => {
        if (response.items > 0) this.resales = response.resales;
      },
      error => console.error(error)
    );
    
    //SET MASKS
    $('#cep').mask('00000-000', {clearIfNotMatch: true});
    
    //ICHECK FOR CHECKBOX AND RADIO INPUTS
    $('input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_square-blue'
    });
    $('input[type="checkbox"].application-features').on('ifChanged', (event) => {
      this.data.application_features[$(event.currentTarget).attr('name')] = $(event.currentTarget).is(':checked');
    });
    $('input[type="checkbox"].communication-type').on('ifChanged', (event) => {
      this.data.communication_type[$(event.currentTarget).attr('name')] = $(event.currentTarget).is(':checked');
    });
  }

  addDNS() {
    this.data.dns.push({
      dns: '',
      ip: ''
    })
  }

  removeDNS(index) {
    this.data.dns.splice(index, 1);
  }

  syncDNS(index) {
    if (this.data.dns[index].dns == '') {
      return;
    }

    this.consominiosService.syncDNS(this.data.dns[index].dns).subscribe(
      response => {
        this.data.dns[index].ip = response.ip;
      },
      error => console.error(error)
    );
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required, select.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($("#gates").val() == '' || $("#gates").val() <= 0) {
      $("#gates").parent().addClass('has-error');
    }

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    this.saving = true;
    this.consominiosService.save(this.data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.saving = false;
        this.router.navigate(['/admin/condominios']);
      },
      error => {
        console.error(error);
        this.saving = false;
      }
    );
  }

  remove() {
    this.consominiosService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/condominios']);
      },
      error => console.error(error)
    );    
  }

  getAddress() {
    if (this.data.cep.length == 9) {
      this.addressLoading = true;
      this.addressService.getAddressByCEP(this.data.cep).subscribe(
        response => {
          this.data.address = response.logradouro;
          this.data.district = response.bairro;
          this.data.city = response.localidade;
          this.data.state = response.uf;
          this.addressLoading = false;
        },
        error => {
          console.error(error);
          this.addressLoading = false;
        }
      );
    }
  }

  getLatLng() {
    if (this.data.address != '' && this.data.number != '') {
      let address = `${this.data.address},${this.data.number},${this.data.district},${this.data.city}`;
      this.addressService.getLatLng(address).subscribe(
        response => {
          if (response.results[0].locations.length > 0) {
            this.data.lat = response.results[0].locations[0].latLng.lat;
            this.data.lng = response.results[0].locations[0].latLng.lng;
          }
        },
        error => {
          console.error(error);
        }
      );
    }
  }  
}
