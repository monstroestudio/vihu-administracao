import { TestBed, inject } from '@angular/core/testing';

import { CondominiosService } from './condominios.service';

describe('CondominiosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CondominiosService]
    });
  });

  it('should be created', inject([CondominiosService], (service: CondominiosService) => {
    expect(service).toBeTruthy();
  }));
});
