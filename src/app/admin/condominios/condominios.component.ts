import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { CondominiosService } from './condominios.service';

declare var $:any;

@Component({
  selector: 'app-condominios',
  templateUrl: './condominios.component.html',
  styleUrls: ['./condominios.component.scss'],
  providers: [CondominiosService]
})
export class CondominiosComponent implements OnInit {

  condominios:any[]=[];

  constructor(private router:Router, private condominiosService:CondominiosService) { }

  ngOnInit() {
    this.condominiosService.list().subscribe(
        response => {
          if (response.items > 0) this.condominios = response.data;
          setTimeout(()=>{ $("#condominios").DataTable(); }, 100);
        },
        error => console.error(error)
    );
  }

  open(item) {
    this.router.navigate(['/admin/condominios/'+item.id]);
  }  
}
