import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevendaFormComponent } from './revenda-form.component';

describe('RevendaFormComponent', () => {
  let component: RevendaFormComponent;
  let fixture: ComponentFixture<RevendaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevendaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevendaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
