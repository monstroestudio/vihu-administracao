import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { RevendasService } from './../revendas.service';
import { AddressService } from './../../../shared/address/address.service';

declare var $:any;

@Component({
  selector: 'app-revenda-form',
  templateUrl: './revenda-form.component.html',
  styleUrls: ['./revenda-form.component.scss'],
  providers: [RevendasService, AddressService]
})
export class RevendaFormComponent implements OnInit {

  data:any = {
    social_name: "",
    name: "",
    cnpj: "",
    cep: "",
    address: "",
    number: "",
    complement: "",
    district: "",
    city: "",
    state:"",
    country: "Brasil",
    site: "",
    status: 1
  };
  addressLoading:boolean;

  constructor(private revendasService:RevendasService, private addressService:AddressService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.revendasService.show(params.id).subscribe(
            response => {
              this.data = response.resale;
              this.data.status = (this.data.status == 'ATIVO') ? 1 : 2;
            },
            error => console.error(error)
          );
        }
      }
    );
  }

  ngOnInit() {
    $('#cnpj').mask('00.000.000/0000-00', {clearIfNotMatch: true});
    $('#cep').mask('00000-000', {clearIfNotMatch: true});
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    this.revendasService.save(this.data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/revendas']);
      },
      error => console.error(error)
    );
  }

  remove() {
    this.revendasService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/revendas']);
      },
      error => console.error(error)
    );    
  }

  getAddress() {
    if (this.data.cep.length == 9) {
      this.addressLoading = true;
      this.addressService.getAddressByCEP(this.data.cep).subscribe(
        response => {
          this.data.address = response.logradouro;
          this.data.district = response.bairro;
          this.data.city = response.localidade;
          this.data.state = response.uf;
          this.addressLoading = false;
        },
        error => {
          console.error(error);
          this.addressLoading = false;
        }
      );
    }
  }
}
