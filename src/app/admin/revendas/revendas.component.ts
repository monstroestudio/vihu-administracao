import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RevendasService } from './revendas.service';

declare var $:any;

@Component({
  selector: 'app-revendas',
  templateUrl: './revendas.component.html',
  styleUrls: ['./revendas.component.scss'],
  providers: [RevendasService]
})
export class RevendasComponent implements OnInit {

  revendas:any[] = [];

  constructor(private router:Router, private revendasService:RevendasService) { }

  ngOnInit() {
    this.revendasService.list().subscribe(
      response => {
        if (response.items > 0) this.revendas = response.resales;
        setTimeout(()=>{ $("#revendas").DataTable(); }, 100);
      },
      error => console.error(error)
    )
  }

  open(item) {
    this.router.navigate(['/admin/revendas/'+item.id]);
  }

}
