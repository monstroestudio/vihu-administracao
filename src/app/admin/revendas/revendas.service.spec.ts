import { TestBed, inject } from '@angular/core/testing';

import { RevendasService } from './revendas.service';

describe('RevendasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RevendasService]
    });
  });

  it('should be created', inject([RevendasService], (service: RevendasService) => {
    expect(service).toBeTruthy();
  }));
});
