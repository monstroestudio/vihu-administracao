import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevendasComponent } from './revendas.component';

describe('RevendasComponent', () => {
  let component: RevendasComponent;
  let fixture: ComponentFixture<RevendasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevendasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevendasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
