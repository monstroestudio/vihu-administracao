import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../shared/guard/auth.guard';
import { AdminComponent } from './admin.component';
import { RevendasComponent } from './revendas/revendas.component';
import { RevendaFormComponent } from './revendas/revenda-form/revenda-form.component';
import { CondominiosComponent } from './condominios/condominios.component';
import { CondominioFormComponent } from './condominios/condominio-form/condominio-form.component';
import { BlocosComponent } from './blocos/blocos.component';
import { BlocoFormComponent } from './blocos/bloco-form/bloco-form.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { UnidadeFormComponent } from './unidades/unidade-form/unidade-form.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioFormComponent } from './usuarios/usuario-form/usuario-form.component';
import { MoradoresComponent } from './moradores/moradores.component';
import { MoradoresFormComponent } from './moradores/moradores-form/moradores-form.component';
import { LogAcessoComponent } from './relatorios/log-acesso/log-acesso.component';
import { LogMoradoresComponent } from './relatorios/log-moradores/log-moradores.component';

const routes: Routes = [
  {
    path:'admin',
    component:AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {path:'revendas', component:RevendasComponent},
      {path:'revendas/novo', component:RevendaFormComponent},
      {path:'revendas/:id', component:RevendaFormComponent},
      {path:'condominios', component:CondominiosComponent},
      {path:'condominios/novo', component:CondominioFormComponent},
      {path:'condominios/:id', component:CondominioFormComponent},
      {path:'blocos', component:BlocosComponent},
      {path:'blocos/novo', component:BlocoFormComponent},
      {path:'blocos/:id', component:BlocoFormComponent},
      {path:'unidades', component:UnidadesComponent},
      {path:'unidades/novo', component:UnidadeFormComponent},
      {path:'unidades/:id', component:UnidadeFormComponent},
      {path:'usuarios', component:UsuariosComponent},
      {path:'usuarios/novo', component:UsuarioFormComponent},
      {path:'usuarios/:id', component:UsuarioFormComponent},
      {path:'moradores', component:MoradoresComponent},
      {path:'moradores/novo', component:MoradoresFormComponent},
      {path:'moradores/:id', component:MoradoresFormComponent},
      {path:'relatorios/acessos', component:LogAcessoComponent},
      {path:'relatorios/moradores', component:LogMoradoresComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
