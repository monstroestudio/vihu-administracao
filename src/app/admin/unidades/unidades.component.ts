import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UnidadesService } from './unidades.service';

declare var $:any;

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.scss'],
  providers: [UnidadesService]
})
export class UnidadesComponent implements OnInit {

  unidades:any[] = [];

  constructor(private router:Router, private unidadesService:UnidadesService) { }

  ngOnInit() {
    this.unidadesService.list().subscribe(
        response => {
          if (response.items > 0) this.unidades = response.data;
          setTimeout(()=>{ $("#unidades").DataTable(); }, 100);
        },
        error => console.error(error)
    );
  }

  open(item) {
    this.router.navigate(['/admin/unidades/'+item.id]);
  }
}
