import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { CondominiosService } from './../../condominios/condominios.service';
import { BlocosService } from './../../blocos/blocos.service';
import { UnidadesService } from './../unidades.service';

declare var $:any;

@Component({
  selector: 'app-unidade-form',
  templateUrl: './unidade-form.component.html',
  styleUrls: ['./unidade-form.component.scss'],
  providers: [BlocosService, UnidadesService, CondominiosService]
})
export class UnidadeFormComponent implements OnInit {

  data:any = {
    block_id: '',
    condominiums_id: '',
    number: '',
    extension_number: '',
    rfid_garage_keys: [{code: ''}],
    vehicles:[]
  }
  blocos:any[] = [];
  condominios:any[] = [];

  constructor(private blocosService:BlocosService, private unidadesService:UnidadesService, private condominiosService:CondominiosService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.unidadesService.show(params.id).subscribe(
            response => {
              this.data = response.unit;
              this.getBlocks();
            },
            error => console.error(error)
          );
        }
      }
    );    
  }

  ngOnInit() {
    this.condominiosService.list().subscribe(
      response => {
        if (response.items > 0) this.condominios = response.data;
      },
      error => console.error(error)
    );    
  }

  getBlocks() {
    if (this.data.condominiums_id == '') {
      this.blocos = [];
      return;
    }

    this.blocosService.listByCondominium(this.data.condominiums_id).subscribe(
      response => {
        this.blocos = response.blocks;
      },
      error => console.error(error)
    );
  }

  addGarageKey() {
    if (!this.data.rfid_garage_keys) this.data.rfid_garage_keys = [];
    this.data.rfid_garage_keys.push({code:''});
  }

  removeGarageKey(index) {
    this.data.rfid_garage_keys.splice(index, 1);
  }  

  addVehicle() {
    if (!this.data.vehicles) this.data.vehicles = [];
    this.data.vehicles.push({
      automaker:'',
      model: '',
      color: '',
      license_plate: ''
    })
  }

  removeVehicle(index) {
    this.data.vehicles.splice(index, 1);
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required, select.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    this.unidadesService.save(this.data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/unidades']);
      },
      error => console.error(error)
    );
  }

  remove() {
    this.unidadesService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/unidades']);
      },
      error => console.error(error)
    );    
  }
}
