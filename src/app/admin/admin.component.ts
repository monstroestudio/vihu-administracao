import { Component, OnInit } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("body").removeClass('login-page');
    $(window).trigger('resize');
  }

}
