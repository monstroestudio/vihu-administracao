import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { RevendasService } from '../../revendas/revendas.service';
import { CondominiosService } from './../../condominios/condominios.service';
import { BlocosService } from './../../blocos/blocos.service';
import { UnidadesService } from '../../unidades/unidades.service';
import { MoradoresService } from '../moradores.service';

declare var $:any;

@Component({
  selector: 'app-moradores-form',
  templateUrl: './moradores-form.component.html',
  styleUrls: ['./moradores-form.component.scss'],
  providers: [RevendasService, CondominiosService, BlocosService, UnidadesService, MoradoresService]
})
export class MoradoresFormComponent implements OnInit {

  data:any = {
    name: '',
    password: '',
    email: '',
    mobile: '',
    phone: '',
    units: [{
      block_id: '',
      condominiums_id: ''
    }]
  }
  condominios:any[] = [];  
  blocos:any[] = [];
  unidades:any[] = [];
  saving:boolean;

  constructor(
    private revendasService:RevendasService, 
    private condominiosService:CondominiosService, 
    private blocosService:BlocosService, 
    private unidadesService:UnidadesService, 
    private moradoresService:MoradoresService,
    private router:Router,
    private route:ActivatedRoute
  ) {
    this.route.params.subscribe( 
      params => {
        if (params.id) {
          this.moradoresService.show(params.id).subscribe(
            response => {
              this.data = response.user;
              if (this.data.units.length == 0){
                this.addUnit();
              }
            },
            error => console.error(error)
          );
        }
      }
    );
  }

  ngOnInit() {
    this.condominiosService.list().subscribe(
      response => {
        this.condominios = response.data;
      },
      error => console.error(error)
    );

    this.blocosService.list().subscribe(
      response => {
        this.blocos = response.blocks;
      },
      error => console.error(error)
    );

    this.unidadesService.list().subscribe(
      response => {
        this.unidades = response.data;
      },
      error => console.error(error)
    );
  }

  filterBlocks(unit) {
    if (unit == '') return [];

    return this.blocos.filter((v) => {
      if (v.condominiums_id == unit.condominiums_id)
        return true;
      else
        return false;
    });
  }

  filterUnits(unit) {
    if (unit == '') return [];

    return this.unidades.filter((v) => {
      if (v.block_id == unit.block_id && v.condominiums_id == unit.condominiums_id) 
        return true;
      else
        return false;
    });
  }

  addUnit() {
    this.data.units.push({
      id: '',
      block_id: '',
      condominiums_id: ''
    });    
  }

  removeUnit(index) {
    this.data.units.splice(index, 1);
  }

  save() {
    $('.form-group.has-error').removeClass('has-error');

    $('input.required, select.required').each(function(index, item){
      if (item.value == '') {
        $(item).parent().addClass('has-error');
      }
    });

    if ($('.form-group.has-error').length > 0) {
      return;
    }

    this.saving = true;
    this.moradoresService.save(this.data).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.Message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });

        this.saving = false;
        this.router.navigate(['/admin/usuarios']);
      },
      error => {
        this.saving = false;
        console.error(error);
      }
    );
  }

  remove() {
    this.moradoresService.remove(this.data.id).subscribe(
      response => {
        $.toast({
          heading: 'Success',
          text: response.message,
          showHideTransition: 'slide',
          position: 'bottom-right',
          icon: 'success'
        });
        this.router.navigate(['/admin/usuarios']);
      },
      error => console.error(error)
    );    
  }

}
