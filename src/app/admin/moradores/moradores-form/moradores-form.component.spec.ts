import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoradoresFormComponent } from './moradores-form.component';

describe('MoradoresFormComponent', () => {
  let component: MoradoresFormComponent;
  let fixture: ComponentFixture<MoradoresFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoradoresFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoradoresFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
