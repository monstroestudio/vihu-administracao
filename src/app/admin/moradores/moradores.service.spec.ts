import { TestBed, inject } from '@angular/core/testing';

import { MoradoresService } from './moradores.service';

describe('MoradoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoradoresService]
    });
  });

  it('should be created', inject([MoradoresService], (service: MoradoresService) => {
    expect(service).toBeTruthy();
  }));
});
