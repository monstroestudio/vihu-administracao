import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MoradoresService } from './moradores.service';

declare var $:any;

@Component({
  selector: 'app-moradores',
  templateUrl: './moradores.component.html',
  styleUrls: ['./moradores.component.scss'],
  providers: [MoradoresService]
})
export class MoradoresComponent implements OnInit {

  moradores:any[] = [];
  
  constructor(private router:Router, private moradoresService:MoradoresService) { }

  ngOnInit() {
    this.moradoresService.list().subscribe(
        response => {
          this.moradores = response.users;
          setTimeout(()=>{ $("#moradores").DataTable(); }, 100);
        },
        error => console.error(error)
    );
  }

  open(item) {
    this.router.navigate(['/admin/moradores/'+item.id]);
  }

}
