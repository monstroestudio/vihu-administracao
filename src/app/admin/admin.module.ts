import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PhonePipe } from './../shared/pipe/phone.pipe';

import { AdminRoutingModule } from './admin-routing.module';
import { CondominiosComponent } from './condominios/condominios.component';
import { CondominioFormComponent } from './condominios/condominio-form/condominio-form.component';
import { RevendasComponent } from './revendas/revendas.component';
import { RevendaFormComponent } from './revendas/revenda-form/revenda-form.component';
import { BlocosComponent } from './blocos/blocos.component';
import { BlocoFormComponent } from './blocos/bloco-form/bloco-form.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { UnidadeFormComponent } from './unidades/unidade-form/unidade-form.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioFormComponent } from './usuarios/usuario-form/usuario-form.component';
import { LogAcessoComponent } from './relatorios/log-acesso/log-acesso.component';
import { MoradoresComponent } from './moradores/moradores.component';
import { MoradoresFormComponent } from './moradores/moradores-form/moradores-form.component';
import { LogMoradoresComponent } from './relatorios/log-moradores/log-moradores.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule
  ],
  declarations: [
    CondominiosComponent, 
    CondominioFormComponent, 
    RevendasComponent, 
    RevendaFormComponent, 
    BlocosComponent, 
    BlocoFormComponent, 
    UnidadesComponent, 
    UnidadeFormComponent, 
    UsuariosComponent, 
    UsuarioFormComponent, 
    LogAcessoComponent, 
    MoradoresComponent, 
    MoradoresFormComponent, 
    LogMoradoresComponent,
    PhonePipe
  ]
})
export class AdminModule { }
