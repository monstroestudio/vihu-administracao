import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AddressService {

  private headers:Headers;

  constructor(private http:Http) {
     this.headers = new Headers();
  }

  getAddressByCEP(cep:string){
    cep = cep.replace('-', '');

		return this.http.get(`https://viacep.com.br/ws/${cep}/json/unicode/`, {
			headers: this.headers
		}).map( response => response.json() );
  }
  
  getLatLng(address:string) {
    let key='EhsuGL4gG10hRvK329XzFb4MqF17unLD';

    return this.http.get(`https://open.mapquestapi.com/geocoding/v1/address?key=${key}&location=${address}&maxResults=1`, {
			headers: this.headers
		}).map( response => response.json() );
  }
}
