import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AppComponent } from '../../app.component';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private router:Router){}

  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (AppComponent.user) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
