import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'phone'
})
export class PhonePipe{
  transform(tel, args)
  {
      var value = tel.toString().trim().replace(/^\+/, '');

      if (value.match(/[^0-9]/)) {
          return tel;
      }

      let city, number;

      switch (value.length) {
          case 11: // +CCCPP####### -> CCC (PP) ###-####
              city = value.slice(0, 2);
              number = value.slice(2);
              break;

          default:
              return tel;
      }

      number = number.slice(0, 5) + '-' + number.slice(5);

      return ("(" + city + ") " + number).trim();
  }
}