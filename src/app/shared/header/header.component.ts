import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user:any;

  constructor() { }

  ngOnInit() {
    this.user = AppComponent.user;
  }
}
